When `module-info.java` is non-empty, IntelliJ marks everything in `build.gradle.kts` red with the error:

```
Symbol is declared in unnamed module which is not read by current module
```

`./gradlew` usage doesn't have any issues, though.

If the contents of `module-info.java` are commented out, the errors go away.

Gradle: 5.2.1
IntelliJ: IU-191-5849.21
